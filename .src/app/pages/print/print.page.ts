import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import EscPosEncoder from 'esc-pos-encoder-ionic';

@Component({
  selector: 'app-print',
  templateUrl: './print.page.html',
  styleUrls: ['./print.page.scss'],
})
export class PrintPage implements OnInit {

  encoder = new EscPosEncoder();
  result = this.encoder.initialize()
    .text('The quick brown fox jumps over the lazy dog')
    .newline()
    .align('center')
    .qrcode('12345678')

    .align('right')
    .line('aligned the right')
    .align('center')
    .line('This line is centered')
    .align('left')
    .line('aligned the left')

    .newline()
    .encode(); // return Uint8Array format
  u8 = new Uint8Array(this.result);
  decoder = new TextDecoder('utf8');
  b64encoded = btoa(this.decoder.decode(this.u8));

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }

  sanitize() {
    // Print use Sanitize link
    const urlintent: string = 'rawbt:base64,'
    this.sanitizer.bypassSecurityTrustUrl(urlintent + this.b64encoded);
  };

  openWindow() {
    // Print use Window Open
    const urlintent: string = 'rawbt:base64,'
    window.open(urlintent + this.b64encoded)
  }

}
