import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  data: any;
  dtOptions: DataTables.Settings = {};

  constructor(public storage: Storage, public barcodeScanner: BarcodeScanner) {

    this.dtOptions = {
      "pagingType": 'full_numbers',
      // scrollY: '50vh',
      "scrollY": "200px",
      "scrollCollapse": true,
    };

    setTimeout(() => {
      document.getElementsByClassName("dataTables_scrollHeadInner")[0].setAttribute("style", "")
      document.getElementsByClassName("row-border hover dataTable no-footer")[0].setAttribute("style", "")
    }, 3000);

    // document.getElementsByClassName("row-border hover dataTable no-footer")[0].setAttribute("witdh", "100%;")

  }

  scan() {
    this.data = null;
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.data = JSON.stringify(barcodeData)
      // this.data = barcodeData;
    }).catch(err => {
      console.log('Error', err);
    });
  }

  public async set(settingName, value) {
    if (await this.get(settingName) == 'blue') {
      alert('sudah ada data');
    } else {
      alert('data kososng')
      return this.storage.set(`setting:${settingName}`, value);
    }
  }

  public async get(settingName) {
    const getData: string = await this.storage.get(`setting:${settingName}`);
    alert(getData);
    return getData
  }

  public async remove(settingName) {
    return await this.storage.remove(`setting:${settingName}`);
  }
  // public clear() {
  //   this.storage.clear().then(() => {
  //     console.log('all keys cleared');
  //   });
  // }

}
