import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { IonicStorageModule } from '@ionic/storage';
import { DataTablesModule } from 'angular-datatables';
// import { BarcodeScanner } from '@ionic-native/barcode-scanner';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    // BarcodeScanner, 
    DataTablesModule,
    IonicStorageModule.forRoot()
  ],
  declarations: [HomePage]
})
export class HomePageModule { }
