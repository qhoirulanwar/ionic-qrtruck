import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { throwError } from 'rxjs';
import { Storage } from '@ionic/storage';
import { StorageService } from '../storage/storage.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiEndPoint: string = "";
  token: string = "";

  constructor(private storage: Storage, private storageService: StorageService, private http: HttpClient) {
    this.apiEndPoint = environment.serverUrl
    this.storageService.getToken().then((data: any) => {
      this.token = data
    })
  }

  httpHeader() {
    return {
      headers: new HttpHeaders({
        Authorization: "Bearer " + this.token
      })
    }
  }

  async httpOptions() {
    const data = await this.storageService.getToken()
    this.token = data
    return {
      headers: new HttpHeaders({
        Authorization: "Bearer " + data
      })
    }
  }

  handleError(error: HttpErrorResponse) {
    // let errorMessage = 'Unknown error!';

    // if (error.error instanceof ErrorEvent) {
    //   // Client-side errors
    //   errorMessage = `Error: ${error.error.message}`;
    // } else {
    //   // Server-side errors
    //   errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    // }

    // if (error.error.message === Object) {
    //   alert(error.error.message)
    // } else {
    //   alert(error.error.message[0].message)
    // }

    // window.alert(errorMessage);

    console.log(error.status.toString().charAt(0));

    if (error.status.toString().charAt(0) == '0') {
      alert('Periksa Jaringan Anda')
    }

    if (error.status.toString().charAt(0) == '4') {
      alert('Silahkan Login Kembali')
      window.location.href = '/';
    }

    if (error.status.toString().charAt(0) == '5') {
      alert('Server Sedang Gangguan')
    }

    return throwError(error);
  };

  public deleteOne(id, table) {
    return this.http.delete(`${this.apiEndPoint}/${table}/${id}`, this.httpHeader())
      .pipe(catchError(this.handleError))
  };

  public noDelete() {
    alert('Data Relasi tidak bisa dihapus')
  };

}
