import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ApiService } from '../http/api.service';

@Injectable({
  providedIn: 'root'
})
export class CorporateService {

  constructor(private http: HttpClient, private api: ApiService) { }

  public addCorp(data) {
    return this.http.post(`${this.api.apiEndPoint}/perusahaans`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public editCorp(id, data) {
    return this.http.put(`${this.api.apiEndPoint}/perusahaans/${id}`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getCorp(): Observable<any> {
    return this.http.get(`${this.api.apiEndPoint}/perusahaans`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getCorpById(id) {
    return this.http.get(`${this.api.apiEndPoint}/perusahaans/${id}`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };
}
