import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ApiService } from '../http/api.service';

@Injectable({
  providedIn: 'root'
})
export class TruckService {

  constructor(private http: HttpClient, private api: ApiService) { }


  public addTruck(data) {
    return this.http.post(`${this.api.apiEndPoint}/truks`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getTruck() {
    return this.http.get(`${this.api.apiEndPoint}/truks`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getTruckByCorp(id) {
    return this.http.get(`${this.api.apiEndPoint}/truks?perusahaan=${id}`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getTruckbyQR(qrcode) {
    return this.http.get(`${this.api.apiEndPoint}/truks?qrcode=${qrcode}`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  getRandomString(length) {
    const randomChars = '0123456789';
    let result = '';
    for (let i = 0; i < length; i++) {
      result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
  };

}
