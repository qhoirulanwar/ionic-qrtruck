import { TestBed } from '@angular/core/testing';

import { TrxscanService } from './trxscan.service';

describe('TrxscanService', () => {
  let service: TrxscanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrxscanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
