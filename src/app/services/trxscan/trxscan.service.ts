import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ApiService } from '../http/api.service';

@Injectable({
  providedIn: 'root'
})
export class TrxscanService {

  constructor(private http: HttpClient, private api: ApiService) { }

  public addTrxScan(data) {
    return this.http.post(`${this.api.apiEndPoint}/scans`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getScansReport() {
    return this.http.get(`${this.api.apiEndPoint}/scans?_sort=created_at`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getReportByDate(gteDate, lteDate) {
    // gteDate or lteDate like 2019-08-14
    // https://strapi.samuderapratamamandiri.co.id/scans?created_at_gt=2019-08-14T00:00:00&created_at_lte=2019-08-14T23:59:59
    return this.http.get(`${this.api.apiEndPoint}/scans?created_at_gte=${gteDate}T00:00:00&created_at_lte=${lteDate}T23:59:59&_limit=10000`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };


}
