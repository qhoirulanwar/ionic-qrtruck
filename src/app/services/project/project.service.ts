import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ApiService } from '../http/api.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient, private api: ApiService) { }

  public addProject(data) {
    return this.http.post(`${this.api.apiEndPoint}/projects`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public editProject(id, data) {
    return this.http.put(`${this.api.apiEndPoint}/projects/${id}`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getProject(): Observable<any> {
    return this.http.get(`${this.api.apiEndPoint}/projects`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getProjectById(id) {
    return this.http.get(`${this.api.apiEndPoint}/projects/${id}`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };


}
