import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { catchError } from 'rxjs/operators';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private api: ApiService, private storage: Storage) { }

  public login(body) {
    return this.http.post(`${this.api.apiEndPoint}/auth/local`, body).pipe(catchError(this.api.handleError))
  };

  public editChecker(id, data) {
    return this.http.put(`${this.api.apiEndPoint}/users/${id}`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  // public async getChecker() {
  //   return this.http.get(`${this.api.apiEndPoint}/users?role=3`, await this.api.httpOptions())
  //     .pipe(catchError(this.api.handleError))
  // };

  public getChecker() {
    return this.http.get(`${this.api.apiEndPoint}/users?role=3`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public async addChecker(data) {
    return this.http.post(`${this.api.apiEndPoint}/users`, data, await this.api.httpOptions())
      .pipe(catchError(this.api.handleError))
  };

}
