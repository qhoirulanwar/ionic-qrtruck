import { TestBed } from '@angular/core/testing';

import { KuarriService } from './kuarri.service';

describe('KuarriService', () => {
  let service: KuarriService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KuarriService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
