import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ApiService } from '../http/api.service';

@Injectable({
  providedIn: 'root'
})
export class KuarriService {

  constructor(private http: HttpClient, private api: ApiService) { }

  public addKuari(data) {
    return this.http.post(`${this.api.apiEndPoint}/kuaris`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getKuari() {
    return this.http.get(`${this.api.apiEndPoint}/kuaris`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public getKuariById(id) {
    return this.http.get(`${this.api.apiEndPoint}/kuaris/${id}`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public editKuari(id, data) {
    return this.http.put(`${this.api.apiEndPoint}/kuaris/${id}`, data, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

  public deleteKuari(id) {
    return this.http.delete(`${this.api.apiEndPoint}/kuaris/${id}`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  }

}
