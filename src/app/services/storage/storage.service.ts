import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  user: any
  token: string

  constructor(private storage: Storage) {

    this.getToken().then((data) => {
      this.token = data
    })

    this.getUser().then((data) => {
      this.user = data
    })

  }

  public async getToken() {
    const token = await this.storage.get("token")
    try {
      return token
    } catch (error) {
      console.log(error);
    }
  };

  public async getUser() {
    const token = await this.storage.get("sessionUser")
    try {
      return token.user
    } catch (error) {
      console.log(error);
    }
  };
}
