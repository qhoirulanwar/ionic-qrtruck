import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckerPage } from './checker.page';

describe('CheckerPage', () => {
  let component: CheckerPage;
  let fixture: ComponentFixture<CheckerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
