import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { AddCheckerComponent } from "./add-checker/add-checker.component";

@Component({
  selector: 'app-checker',
  templateUrl: './checker.page.html',
  styleUrls: ['./checker.page.scss'],
})
export class CheckerPage implements OnInit {
  data: any = [];


  constructor(private modalController: ModalController, private userService: UserService) { }

  async ngOnInit() {
    // await (await this.userService.getUsers()).subscribe((data: any) => {
    //   console.log(data);
    // })
    this.getData()
  }

  async getData() {
    await (await this.userService.getChecker()).subscribe((data: any) => {
      console.log(data);
      this.data = data
    })
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddCheckerComponent,
      cssClass: 'my-custom-class'
    });
    await modal.present();

    let { data } = await modal.onWillDismiss();
    console.log(data);

    // refresh after modal 
    if (data != undefined) {
      alert('data berhasil disimpan')
      this.getData()
    }
  }

  async ubahDataModal(id) {
    // console.log(data);

    const modal = await this.modalController.create({
      component: AddCheckerComponent,
      componentProps: { id, headTitle: 'Ubah' }
    });
    await modal.present();

    let { data } = await modal.onWillDismiss();
    console.log(data);

    // refresh after modal 
    if (data != undefined) {
      alert('data berhasil disimpan')
      this.getData()
    }
  }

}
