import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-add-checker',
  templateUrl: './add-checker.component.html',
  styleUrls: ['./add-checker.component.scss'],
})
export class AddCheckerComponent implements OnInit {

  headTitle: string = 'Tambah'
  id

  constructor(private modalController: ModalController, private userService: UserService) { }

  ngOnInit() {
    if (this.headTitle == 'Ubah') {
      // isi form dengan data dari halaman sebelum (id)
      this.model = this.id
      console.log(this.id);
    }
  }

  form = new FormGroup({});
  model = {
    "username": null,
    "email": null,
    "phone": null,
    "password": null,
    "confirmed": true,
    "role": "3"
  };
  fields: FormlyFieldConfig[] = [
    {
      key: 'username',
      type: 'input',
      templateOptions: {
        label: 'Username',
        required: true,
        placeholder: 'Masukan Username Checker'
      }
    },
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        label: 'Email',
        placeholder: 'Alamat Email',
        type: 'text'
      }
    },
    {
      key: 'phone',
      type: 'input',
      templateOptions: {
        label: 'No. Telp.',
        placeholder: 'Masukan No. Telphone',
        type: 'tel'
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        label: 'Password',
        placeholder: 'Masukan Password',
        type: 'text'
      }
    }
  ];

  async onSubmit() {
    // await this.form.setValue({ 'confirmed': true })
    console.log(this.form.value);

    if (this.headTitle == 'Tambah') {
      await (await this.userService.addChecker(this.model))
        .subscribe(data => {
          console.log(data)
        });

      this.modalController.dismiss({
        'reload': true
      })
    }

    if (this.headTitle == 'Ubah') {
      this.userService.editChecker(this.id.id, this.model).subscribe((data: any) => {
        console.log(data);
      })

      this.modalController.dismiss({
        'reload': true
      })
    }


    // console.log(this.model);
    // console.log(this.form.valid);
  }

}
