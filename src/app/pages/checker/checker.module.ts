import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckerPageRoutingModule } from './checker-routing.module';

import { CheckerPage } from './checker.page';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyIonicModule } from '@ngx-formly/ionic';
import { AddCheckerComponent } from './add-checker/add-checker.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckerPageRoutingModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      extras: { lazyRender: true },
      validationMessages: [
        { name: 'required', message: '(*) This field is required' },
      ]
    }),
    FormlyIonicModule
  ],
  declarations: [CheckerPage, AddCheckerComponent]
})
export class CheckerPageModule { }
