import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckerScanPage } from './checker-scan.page';

const routes: Routes = [
  {
    path: '',
    component: CheckerScanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckerScanPageRoutingModule {}
