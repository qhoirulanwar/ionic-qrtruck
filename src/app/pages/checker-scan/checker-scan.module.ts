import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckerScanPageRoutingModule } from './checker-scan-routing.module';

import { CheckerScanPage } from './checker-scan.page';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyIonicModule } from '@ngx-formly/ionic';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckerScanPageRoutingModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      extras: { lazyRender: true },
      validationMessages: [
        { name: 'required', message: '(*) This field is required' },
      ]
    }),
    FormlyIonicModule
  ],
  declarations: [CheckerScanPage]
})
export class CheckerScanPageModule { }
