import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CorporateService } from 'src/app/services/corporate/corporate.service';
// import { KuarriService } from 'src/app/services/kuarri/kuarri.service';
import { ProjectService } from 'src/app/services/project/project.service';
import { TruckService } from 'src/app/services/truck/truck.service';
import { TrxscanService } from 'src/app/services/trxscan/trxscan.service';
import { BarcodeScannerOptions, BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import EscPosEncoder from 'esc-pos-encoder-ionic';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ApiService } from 'src/app/services/http/api.service';

@Component({
  selector: 'app-checker-scan',
  templateUrl: './checker-scan.page.html',
  styleUrls: ['./checker-scan.page.scss'],
})
export class CheckerScanPage implements OnInit {
  encoder = new EscPosEncoder();
  barcodeScannerOptions: BarcodeScannerOptions;
  form = new FormGroup({});
  fields: FormlyFieldConfig[]

  resultTruck: any
  resultPerusahaan: any
  resultkuari: any
  resultProject: any

  showFooter: boolean = false
  showTrx: boolean = false

  waktu: string;
  scannedData: any

  modelTrx = {
    "transaksi": Date.now().toString(),
    "truk": null,
    "kuari": null,
    "perusahaan": null,
    "project": null,
    "note": null,
    "created_by": null
  }

  // model = {
  //   perusahaan: null
  // };

  onSubmit(model) {
  }

  constructor(
    private platform: Platform,
    private http: HttpClient,
    private api: ApiService,
    private barcodeScanner: BarcodeScanner,
    private storageSrvc: StorageService,
    private truckSrvc: TruckService,
    private corpSrvc: CorporateService,
    private trxscanSrvc: TrxscanService,
    private projectSrvc: ProjectService
  ) {
    //Options
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }

  ngOnInit() {
    this.scannedData = this.scannedNullModel()

    // set form field 
    setTimeout(() => {
      this.fields = [
        {
          key: 'perusahaan',
          type: 'select',
          templateOptions: {
            label: 'Project',
            placeholder: 'Pilih Project',
            type: 'text',
            required: true,
            options: this.projectSrvc.getProject(),
            valueProp: 'id',
            labelProp: 'Name',
            change: (field, $event) => {
              // console.log(field.formControl.value);
              // console.log($event.detail.value);
              this.projectSrvc.getProjectById(field.formControl.value).subscribe((data) => {
                this.resultProject = data
              })
            },
          }
        }
      ]
    }, 3000);

  };

  scanCheckTruck() {
    const deviceWeb = this.platform.is('desktop')
    const deviceMobileWeb = this.platform.is('mobileweb')
    console.log(this.form.valid);

    if (!this.form.valid) {
      alert('Anda belum memilih Project')
    } else {

      if (deviceWeb || deviceMobileWeb) {
        this.getdataTruck("008d46ef6684536f9d107752")
      } else {
        this.openQRScan()
      }

    }
  };

  openQRScan() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        // alert("Barcode data " + JSON.stringify(barcodeData.text));
        // this.scannedData = barcodeData;
        this.getdataTruck(barcodeData.text)

      })
      .catch(err => {
        console.log("Error", err);
      });
  };

  getdataTruck(qrTruck: string) {
    this.truckSrvc.getTruckbyQR(qrTruck).subscribe((data: any) => {
      // console.log(data);
      this.resultTruck = data[0]
      this.scannedData = data[0]
      this.showFooter = true

      this.getPerusahaan(data[0].perusahaan.id)
    })
  }

  getPerusahaan(id) {
    this.corpSrvc.getCorpById(id).subscribe((data: any) => {
      this.resultPerusahaan = data
      // console.log(data);

      this.resultkuari = data.kuari
      // this.resultProject = data.project


      console.log(this.resultTruck);
      console.log(this.resultPerusahaan);
      console.log(this.resultkuari);
      console.log(this.resultProject);
    })
  }

  // getKuari(id) {
  //   this.kuariSrvc.getKuariById(id).subscribe((data: any) => {
  //     this.resultkuari = data
  //     // console.log(data);
  //   })
  // }

  // getProject(id) {
  //   this.projectSrvc.getProjectById(id).subscribe((data: any) => {
  //     this.resultProject = data[0]
  //     // console.log(data[0]);
  //   })
  // }

  processtrx() {
    this.modelTrx.truk = this.resultTruck.id
    this.modelTrx.kuari = this.resultkuari.id
    this.modelTrx.perusahaan = this.resultPerusahaan.id
    this.modelTrx.project = this.resultProject.id
    this.modelTrx.created_by = this.storageSrvc.user.id.toString()
    this.modelTrx.note = {
      "truk": this.resultTruck,
      "kuari": this.resultkuari,
      "perusahaan": this.resultPerusahaan,
      "project": this.resultProject
    }

    this.trxscanSrvc.addTrxScan(this.modelTrx).subscribe((data) => {
      console.log(data);
      this.printRecipt()

      // kosongkan data
      this.scannedData = this.scannedNullModel()
      this.showFooter = false
      // alert('Berhasil')
    })
  };

  scannedNullModel() {
    return {
      "id": null,
      "nopol": null,
      "supir": null,
      "perusahaan": {
        "id": null,
        "name": null,
        "alamat": null,
        "pic": null,
        "telp": null,
        "project": null,
        "kuari": null,
        "created_by": null,
        "updated_by": null
      },
      "index": null,
      "panjang": null,
      "lebar": null,
      "tinggi": null,
      "qrcode": null,
      "created_by": {
        "id": null,
        "firstname": null,
        "lastname": null,
        "username": null,
        "email": null,
        "password": null,
        "resetPasswordToken": null,
        "registrationToken": null,
        "isActive": true,
        "roles": [
          null
        ],
        "blocked": true
      },
      "updated_by": {
        "id": null,
        "firstname": null,
        "lastname": null,
        "username": null,
        "email": null,
        "password": null,
        "resetPasswordToken": null,
        "registrationToken": null,
        "isActive": true,
        "roles": [
          null
        ],
        "blocked": true
      }
    }
  }

  printRecipt() {
    const waktu = new Date()
    const year = waktu.getFullYear();
    const month = waktu.getMonth(); // January is 0 not 1
    const tgl = waktu.getDate();
    const jam = waktu.getHours();
    const menit = waktu.getMinutes();

    const dateString = tgl + "-" + (month + 1) + "-" + year + " " + jam + ":" + menit;
    this.waktu = dateString

    const result = this.encoder.initialize()
      .newline()
      .align('center')
      .bold(true)
      .line('STRUK SCAN MASUK RITASE')
      .bold(false)
      .line('================================')
      .bold(true)
      .newline()

      .align('left')
      .line('Waktu : ' + dateString)
      // .line('No Trans : ' + this.scannedData.checkId)
      .line('No Trans : ' + this.modelTrx.transaksi)
      .line('--------------------------------')
      .align('center')
      .barcode(this.modelTrx.transaksi, 'itf', 60)
      // .barcode(this.scannedData.checkId, 'itf', 60)

      .align('right')
      .line('Transaksi')
      .line(this.scannedData.perusahaan.name)
      .newline()

      .align('left')
      .line('No.Pol: ' + this.scannedData.nopol)
      .line('Muatan: ' + this.scannedData.index)

      .newline()
      // .align('right')
      // .line('Petugas Checker: ' + this.userSrvc.data.user_nama)
      .line('Petugas Checker: ' + this.storageSrvc.user.username)
      .newline()

      .bold(false)
      .line('================================')
      .newline()
      .align('center')
      .line('STRUK INI MERUPAKAN')
      .line('BUKTI TRANSAKSI SAH')
      .newline()
      // .line('info@smartukm.biz.id')
      .text('Sistem Info 08787-1999-559')
      // .line('Made With Love')
      // .qrcode('123456789')
      // .text(12345678)

      .newline()
      .newline()
      .encode()

    const u8 = new Uint8Array(result);
    const decoder = new TextDecoder('utf8');
    const b64encoded = btoa(decoder.decode(u8));

    const urlintent: string = 'rawbt:base64,'
    window.open(urlintent + b64encoded)
  };

}
