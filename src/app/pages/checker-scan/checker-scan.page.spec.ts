import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckerScanPage } from './checker-scan.page';

describe('CheckerScanPage', () => {
  let component: CheckerScanPage;
  let fixture: ComponentFixture<CheckerScanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckerScanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckerScanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
