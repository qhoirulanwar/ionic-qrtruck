import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { ApiService } from 'src/app/services/http/api.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  projectCount: number = 0
  kuarriCount: number = 0
  perusahaanCount: number = 0
  truckCount: number = 0
  staffCount: number = 0
  ritaseScanCount: number = 0

  constructor(private http: HttpClient, private api: ApiService, private userSrvc: UserService) { }

  ngOnInit() {
    this.getCount('projects').subscribe((data: number) => this.projectCount = data)
    this.getCount('kuaris').subscribe((data: number) => this.kuarriCount = data)
    this.getCount('perusahaans').subscribe((data: number) => this.perusahaanCount = data)
    this.getCount('truks').subscribe((data: number) => this.truckCount = data)
    this.userSrvc.getChecker().subscribe((data: Array<any>) => this.staffCount = data.length)
    this.getCount('scans').subscribe((data: number) => this.ritaseScanCount = data)
  }

  getCount(tableName: String) {
    return this.http.get(`${this.api.apiEndPoint}/${tableName}/count`, this.api.httpHeader())
      .pipe(catchError(this.api.handleError))
  };

}
