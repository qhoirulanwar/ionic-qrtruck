import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { ProjectService } from 'src/app/services/project/project.service';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss'],
})
export class AddProjectComponent implements OnInit {

  headTitle: string = 'Tambah'
  id: { id: any; Name: any; alamat: any; volume: any; catatan: any; ritase: any; };

  constructor(
    public modalController: ModalController,
    public projectSrvc: ProjectService
  ) { }

  ngOnInit() {
    if (this.headTitle == 'Ubah') {
      // isi form dengan data dari halaman sebelum (id)
      this.model = this.id
      console.log(this.id);
    }
  }

  form = new FormGroup({});
  model = {
    Name: null,
    alamat: null,
    volume: null,
    catatan: null,
    ritase: null
  };
  fields: FormlyFieldConfig[] = [
    {
      key: 'Name',
      type: 'input',
      templateOptions: {
        label: 'Nama Project',
        required: true,
        placeholder: 'Masukan Nama Project'
      }
    },
    {
      key: 'alamat',
      type: 'input',
      templateOptions: {
        label: 'Alamat Project',
        placeholder: 'Alamat Project',
        type: 'text'
      }
    },
    {
      key: 'volume',
      type: 'input',
      templateOptions: {
        label: 'Volume Project',
        placeholder: 'Masukan Volume Project',
        type: 'text'
      }
    },
    {
      key: 'ritase',
      type: 'input',
      templateOptions: {
        label: 'Ritase Project',
        placeholder: 'Masukan Ritase Project',
        type: 'number'
      }
    },
    {
      key: 'catatan',
      type: 'textarea',
      templateOptions: {
        label: 'Catatan Project',
        placeholder: 'Masukan Catatan Project',
        type: 'text'
      }
    }
  ];

  onSubmit() {
    console.log(this.model);
    console.log(this.form.valid);

    if (this.form.valid) {
      if (this.headTitle == 'Tambah') {
        this.projectSrvc.addProject(this.model).subscribe((data: any) => {
          console.log(data);
        })

        this.modalController.dismiss({
          'reload': true
        })
      }
    }

    if (this.headTitle == 'Ubah') {
      this.projectSrvc.editProject(this.id.id, this.model).subscribe((data: any) => {
        console.log(data);
      })

      this.modalController.dismiss({
        'reload': true
      })
    }
  }

  // closeModal() {
  //   this.modalController.dismiss();
  // };

}
