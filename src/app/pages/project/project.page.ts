import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProjectService } from 'src/app/services/project/project.service';
import { AddProjectComponent } from './add-project/add-project.component';

@Component({
  selector: 'app-project',
  templateUrl: './project.page.html',
  styleUrls: ['./project.page.scss'],
})
export class ProjectPage implements OnInit {
  data: any = [];

  constructor(public modalController: ModalController, private projectSrvc: ProjectService) { }

  ngOnInit() {
    this.getData()
  }

  async getData() {
    await (await this.projectSrvc.getProject()).subscribe((data: any) => {
      console.log(data);
      this.data = data
    })
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddProjectComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async ubahDataModal(id) {
    // console.log(data);

    const modal = await this.modalController.create({
      component: AddProjectComponent,
      componentProps: { id, headTitle: 'Ubah' }
    });
    await modal.present();

    let { data } = await modal.onWillDismiss();
    console.log(data);

    // refresh after modal 
    if (data != undefined) {
      alert('data berhasil disimpan')
      this.getData()
    }
  }
}
