import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CorporateService } from 'src/app/services/corporate/corporate.service';
import { AddCorporateComponent } from "./add-corporate/add-corporate.component";

@Component({
  selector: 'app-corporation',
  templateUrl: './corporation.page.html',
  styleUrls: ['./corporation.page.scss'],
})
export class CorporationPage implements OnInit {
  data: any = [];

  constructor(private modalController: ModalController, private corporateCtrl: CorporateService) { }

  ngOnInit() {
    this.getData()
  }

  async getData() {
    await (await this.corporateCtrl.getCorp()).subscribe((data: any) => {
      console.log(data);
      this.data = data
    })
  }

  async ubahDataModal(id) {
    // console.log(data);

    const modal = await this.modalController.create({
      component: AddCorporateComponent,
      componentProps: { id, headTitle: 'Ubah' }
    });
    await modal.present();

    let { data } = await modal.onWillDismiss();
    console.log(data);

    // refresh after modal 
    if (data != undefined) {
      alert('data berhasil disimpan')
      this.getData()
    }
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddCorporateComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
