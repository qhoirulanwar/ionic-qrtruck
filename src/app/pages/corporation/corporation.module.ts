import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CorporationPageRoutingModule } from './corporation-routing.module';

import { CorporationPage } from './corporation.page';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyIonicModule } from '@ngx-formly/ionic';
import { AddCorporateComponent } from "./add-corporate/add-corporate.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CorporationPageRoutingModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      extras: { lazyRender: true },
      validationMessages: [
        { name: 'required', message: '(*) This field is required' },
      ]
    }),
    FormlyIonicModule
  ],
  declarations: [CorporationPage, AddCorporateComponent]
})
export class CorporationPageModule { }
