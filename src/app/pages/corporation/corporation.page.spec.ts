import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CorporationPage } from './corporation.page';

describe('CorporationPage', () => {
  let component: CorporationPage;
  let fixture: ComponentFixture<CorporationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CorporationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
