import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CorporateService } from 'src/app/services/corporate/corporate.service';

@Component({
  selector: 'app-add-corporate',
  templateUrl: './add-corporate.component.html',
  styleUrls: ['./add-corporate.component.scss'],
})
export class AddCorporateComponent implements OnInit {

  headTitle: string = 'Tambah'
  id

  constructor(
    public modalController: ModalController,
    public corpSrvc: CorporateService
  ) { }

  ngOnInit() {
    if (this.headTitle == 'Ubah') {
      // isi form dengan data dari halaman sebelum (id)
      this.model = this.id
      console.log(this.id);
    }
  }


  form = new FormGroup({});
  model = {
    name: null,
    alamat: null,
    pic: null,
    telp: null
  };
  fields: FormlyFieldConfig[] = [
    {
      key: 'name',
      type: 'input',
      // defaultValue: 'asu',
      templateOptions: {
        label: 'Nama Perusahaan',
        required: true,
        placeholder: 'Masukan Nama Perusahaan',
        // disabled: true
      }
    },
    {
      key: 'alamat',
      type: 'input',
      templateOptions: {
        label: 'Alamat Perusahaan',
        placeholder: 'Masukan Alamat Perusahaan',
        type: 'text'
      }
    },
    {
      key: 'pic',
      type: 'input',
      templateOptions: {
        label: 'Nama PIC',
        placeholder: 'Masukan Nama PIC',
        type: 'text'
      }
    },
    {
      key: 'telp',
      type: 'input',
      templateOptions: {
        label: 'Telp',
        placeholder: 'Masukan No Telp.',
        type: 'number'
      }
    }
  ];

  onSubmit() {
    // this.model.name = 'udin'

    console.log(this.model);

    if (this.headTitle == 'Tambah') {
      this.corpSrvc.addCorp(this.model).subscribe((data: any) => {
        console.log(data);
      })

      this.modalController.dismiss({
        'reload': true
      })
    }

    if (this.headTitle == 'Ubah') {
      this.corpSrvc.editCorp(this.id.id, this.model).subscribe((data: any) => {
        console.log(data);
      })

      this.modalController.dismiss({
        'reload': true
      })
    }


    // this.fields[0].templateOptions.disabled = true
    // this.fields[0].defaultValue = 'udin'
  }
}
