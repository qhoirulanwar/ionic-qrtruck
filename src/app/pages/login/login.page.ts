import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  screenHeight = (window.innerHeight) + "px";
  model = {
    identifier: null,
    password: null
  };

  constructor(
    public router: Router,
    public userService: UserService,
    public storage: Storage,
    public platform: Platform
  ) { }

  ngOnInit() {
    console.log(this.screenHeight);

  }

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [
    {
      key: 'identifier',
      type: 'input',
      templateOptions: {
        label: 'Username',
        placeholder: 'Masukan username/email'
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        label: 'Password',
        placeholder: 'Enter password',
        type: 'password'
      }
    }
  ];

  login() {
    this.router.navigateByUrl('/app/admin/home', { replaceUrl: true });
  }

  onSubmit() {
    console.log(this.model);

    this.userService.login(this.model).subscribe((data: any) => {
      console.log(data);
      this.storage.set('sessionUser', data)
      this.storage.set('token', data.jwt)

      switch (data.user.role.type) {
        case "checker":
          this.router.navigateByUrl('/scan', { replaceUrl: true });
          break;

        default:
          this.router.navigateByUrl('/app', { replaceUrl: true });
          break;
      }

    }, (error: any) => {
      alert(error.error.message[0].messages[0].message);

    })
  }

}
