import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportPageRoutingModule } from './report-routing.module';

import { ReportPage } from './report.page';
import { DataTablesModule } from 'angular-datatables';
import { DataTablesComponent } from './data-tables/data-tables.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DataTablesModule,
    ReportPageRoutingModule
  ],
  declarations: [
    ReportPage,
    DataTablesComponent
  ]
})
export class ReportPageModule { }
