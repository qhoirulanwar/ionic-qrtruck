import { Component, OnDestroy, OnInit } from '@angular/core';
import { TrxscanService } from 'src/app/services/trxscan/trxscan.service';
import { formatDate } from "@angular/common";
import { Subject } from 'rxjs';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})

// TODO: laporan per perusahaan
// TODO: laporan per perusahaan

export class ReportPage implements OnDestroy, OnInit {

  indexTotal = 0;

  starDate
  endDate
  datatableShow: boolean = false

  data: any;
  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private trxScanSrvc: TrxscanService) {
    // this.getData()
  }

  ngOnInit() {
    // this.setupDataTables()

    this.getData()

    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 2,
    //   // scrollY: '50vh',
    //   scrollY: "80%",
    //   scrollCollapse: true,
    // };

    // this.trxScanSrvc.getScansReport().subscribe((data) => {
    //   console.log(data);
    //   this.data = data
    //   this.dtTrigger.next();
    // })

    // console.log(formatDate("2021-04-19T10:24:59.000Z", 'long', 'en_US', '+0000'))
    // console.log(formatDate("2021-04-19T10:24:59.000Z", 'long', 'en-US', '+0700'))
    // console.log(formatDate("2021-04-19T10:24:59.000Z", 'long', 'en-US', '+1500'))
    // console.log(formatDate("2021-04-19T10:24:59.000Z", 'long', 'en-US', '-0700'))
  }

  DateSelect() {
    this.datatableShow = false

    const start = new Date(this.starDate)
    const end = new Date(this.endDate)
    const startDate = start.getFullYear() + '-' + ("0" + (start.getMonth() + 1)).slice(-2) + '-' + start.getDate()
    const endDate = end.getFullYear() + '-' + ("0" + (end.getMonth() + 1)).slice(-2) + '-' + end.getDate()

    // jika tanggal akhir sudah dipilih
    if (this.endDate === undefined) {
      console.log('tgl akhir belum dipilih');
    } else {

      this.trxScanSrvc.getReportByDate(startDate, endDate).subscribe((data: any) => {
        console.log(data);
        this.data = data
        this.datatableShow = true
      })
    }

  };

  setupDataTables() {
    this.dtOptions = {
      "pagingType": 'full_numbers',
      // scrollY: '50vh',
      "scrollY": "70%",
      "scrollCollapse": true,
    };

    setTimeout(() => {
      document.getElementsByClassName("dataTables_scrollHeadInner")[0].setAttribute("style", "")
      document.getElementsByClassName("row-border hover dataTable no-footer")[0].setAttribute("style", "")
    }, 3000);

  }

  getData() {
    this.trxScanSrvc.getReportByDate('2019-08-14', '2019-08-14').subscribe((data: any) => {
      console.log(data);
      this.data = data
      this.datatableShow = true
      // this.dtTrigger.next();

      for (let i = 0; i < this.data.length; i++) {
        this.indexTotal += this.data[i].index;
      }
      console.log(this.indexTotal.toFixed(1));
      console.log(Math.round(this.indexTotal));

      let counts = []
      for (let i = 0; i < this.data.length; i++) {
        if (counts[this.data[i].perusahaan.name]) {
          counts[this.data[i].perusahaan.name] += 1
        } else {
          counts[this.data[i].perusahaan.name] = 1
        }
      }
      console.log(counts);
    })
  }

  formatWaktu(waktu: string) {
    return formatDate(waktu, 'HH:mm', 'en-US', '+0700')
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}
