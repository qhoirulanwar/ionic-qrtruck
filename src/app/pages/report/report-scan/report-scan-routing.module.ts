import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportScanPage } from './report-scan.page';

const routes: Routes = [
  {
    path: '',
    component: ReportScanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportScanPageRoutingModule {}
