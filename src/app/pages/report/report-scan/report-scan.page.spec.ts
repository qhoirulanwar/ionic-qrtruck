import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportScanPage } from './report-scan.page';

describe('ReportScanPage', () => {
  let component: ReportScanPage;
  let fixture: ComponentFixture<ReportScanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportScanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportScanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
