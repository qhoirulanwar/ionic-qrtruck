import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportScanPageRoutingModule } from './report-scan-routing.module';

import { ReportScanPage } from './report-scan.page';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportScanPageRoutingModule,
    DataTablesModule
  ],
  declarations: [ReportScanPage]
})
export class ReportScanPageModule { }
