// import { formatDate } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-data-tables',
  templateUrl: './data-tables.component.html',
  styleUrls: ['./data-tables.component.scss'],
})
export class DataTablesComponent implements OnInit {

  @Input() data: Array<any>
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  constructor() { }

  ngOnInit() {
    console.log(this.data);
    console.log("woy");

    this.setupDataTables()
    // this.dtTrigger.next();
  }

  setupDataTables() {
    this.dtOptions = {
      "pagingType": 'full_numbers',
      // scrollY: '50vh',
      "scrollY": "70%",
      "scrollCollapse": true,
    }

    setTimeout(() => {
      document.getElementsByClassName("dataTables_scrollHeadInner")[0].setAttribute("style", "")
      document.getElementsByClassName("row-border hover dataTable no-footer")[0].setAttribute("style", "")
    }, 500);
  };

  // formatWaktu(waktu: string) {
  //   return formatDate(waktu, 'HH:mm', 'en-US', '+0700')
  // }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}
