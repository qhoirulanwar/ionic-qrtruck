import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivityProjectPageRoutingModule } from './activity-project-routing.module';

import { ActivityProjectPage } from './activity-project.page';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivityProjectPageRoutingModule,
    DataTablesModule
  ],
  declarations: [ActivityProjectPage]
})
export class ActivityProjectPageModule { }
