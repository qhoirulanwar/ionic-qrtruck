import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-activity-project',
  templateUrl: './activity-project.page.html',
  styleUrls: ['./activity-project.page.scss'],
})
export class ActivityProjectPage implements OnInit {
  startDate: string = null
  endDate: string = null

  customYearValues = [2020, 2016, 2008, 2004, 2000, 1996];
  customDayShortNames = ['s\u00f8n', 'man', 'tir', 'ons', 'tor', 'fre', 'l\u00f8r'];
  customPickerOptions: any;

  constructor() { }

  ngOnInit() {
    this.customPickerOptions = {
      buttons: [{
        text: 'Save',
        handler: () => console.log('Clicked Save!')
      }, {
        text: 'Log',
        handler: () => {
          console.log('Clicked Log. Do not Dismiss.');
          return false;
        }
      }]
    }
  };

  eventStartDate(event: any) {
    this.startDate = event.substring(0, 10)
    console.log(this.startDate);
  }

  eventEndDate(event: any) {
    this.endDate = event.substring(0, 10)
    console.log(this.endDate);
  }

}
