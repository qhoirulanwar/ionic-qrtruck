import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActivityProjectPage } from './activity-project.page';

const routes: Routes = [
  {
    path: '',
    component: ActivityProjectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActivityProjectPageRoutingModule {}
