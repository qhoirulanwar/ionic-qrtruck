import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActivityProjectPage } from './activity-project.page';

describe('ActivityProjectPage', () => {
  let component: ActivityProjectPage;
  let fixture: ComponentFixture<ActivityProjectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityProjectPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActivityProjectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
