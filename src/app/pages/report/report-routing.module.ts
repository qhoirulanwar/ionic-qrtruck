import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportPage } from './report.page';

const routes: Routes = [
  {
    path: '',
    component: ReportPage
  },  {
    path: 'report-scan',
    loadChildren: () => import('./report-scan/report-scan.module').then( m => m.ReportScanPageModule)
  },
  {
    path: 'activity-project',
    loadChildren: () => import('./activity-project/activity-project.module').then( m => m.ActivityProjectPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportPageRoutingModule {}
