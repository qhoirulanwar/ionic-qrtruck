import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KuarriPage } from './kuarri.page';

describe('KuarriPage', () => {
  let component: KuarriPage;
  let fixture: ComponentFixture<KuarriPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KuarriPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KuarriPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
