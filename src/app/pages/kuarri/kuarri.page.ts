import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { KuarriService } from 'src/app/services/kuarri/kuarri.service';
import { AddKuarriComponent } from "./add-kuarri/add-kuarri.component";

@Component({
  selector: 'app-kuarri',
  templateUrl: './kuarri.page.html',
  styleUrls: ['./kuarri.page.scss'],
})
export class KuarriPage implements OnInit {
  data: any = [];

  constructor(private modalController: ModalController, private kuarriSrvc: KuarriService) { }

  ngOnInit() {
    this.getData()
  }

  async getData() {
    await (await this.kuarriSrvc.getKuari()).subscribe((data: any) => {
      console.log(data);
      this.data = data
    })
  }

  async ubahDataModal(id) {
    // console.log(data);

    const modal = await this.modalController.create({
      component: AddKuarriComponent,
      componentProps: { id, headTitle: 'Ubah' }
    });
    await modal.present();

    let { data } = await modal.onWillDismiss();
    console.log(data);

    // refresh after modal 
    if (data != undefined) {
      alert('data berhasil disimpan')
      this.getData()
    }
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddKuarriComponent
    });
    await modal.present();

    let { data } = await modal.onWillDismiss();
    console.log(data);

    // refresh after modal 
    if (data != undefined) {
      alert('data berhasil disimpan')
      this.getData()
    }

    // await modal.onWillDismiss().then((data) => {
    //   console.log(data);
    // })
  };

  hapus(id) {
    console.log(id);
    this.kuarriSrvc.deleteKuari(id).subscribe((data: any) => {
      console.log(data);
      this.getData()
      alert('data berhasil dihapus')
    })
  }

}
