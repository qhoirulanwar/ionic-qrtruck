import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KuarriPage } from './kuarri.page';

const routes: Routes = [
  {
    path: '',
    component: KuarriPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KuarriPageRoutingModule {}
