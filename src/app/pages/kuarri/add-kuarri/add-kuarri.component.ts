import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { KuarriService } from 'src/app/services/kuarri/kuarri.service';

@Component({
  selector: 'app-add-kuarri',
  templateUrl: './add-kuarri.component.html',
  styleUrls: ['./add-kuarri.component.scss'],
})
export class AddKuarriComponent implements OnInit {

  constructor(private modalController: ModalController, private kuarriCtrl: KuarriService) { }

  headTitle: string = 'Tambah'
  id
  form = new FormGroup({});
  model = {
    name: null
  }
  fields: FormlyFieldConfig[] = [
    {
      key: 'name',
      type: 'input',
      templateOptions: {
        label: 'Nama Kuari',
        required: true,
        placeholder: 'Masukan Nama Kuari'
      }
    }
  ]

  ngOnInit() {
    if (this.headTitle == 'Ubah') {
      // isi form dengan data dari halaman sebelum (id)
      this.model = this.id
      console.log(this.id);
    }
  };

  onSubmit() {
    console.log(this.model);
    console.log(this.form.valid);
    if (this.form.valid) {
      console.log('form not valid');
    }


    if (this.headTitle == 'Tambah') {
      this.kuarriCtrl.addKuari(this.model).subscribe((data: any) => {
        console.log(data);
      })

      this.modalController.dismiss({
        'reload': true
      })
    }

    if (this.headTitle == 'Ubah') {
      this.kuarriCtrl.editKuari(this.id.id, this.model).subscribe((data: any) => {
        console.log(data);
      })

      this.modalController.dismiss({
        'reload': true
      })
    }

  }

}
