import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddKuarriComponent } from './add-kuarri.component';

describe('AddKuarriComponent', () => {
  let component: AddKuarriComponent;
  let fixture: ComponentFixture<AddKuarriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddKuarriComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddKuarriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
