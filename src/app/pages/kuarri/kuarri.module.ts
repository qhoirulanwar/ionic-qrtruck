import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KuarriPageRoutingModule } from './kuarri-routing.module';

import { KuarriPage } from './kuarri.page';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyIonicModule } from '@ngx-formly/ionic';
import { AddKuarriComponent } from './add-kuarri/add-kuarri.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KuarriPageRoutingModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      extras: { lazyRender: true },
      validationMessages: [
        { name: 'required', message: '(*) This field is required' },
      ]
    }),
    FormlyIonicModule
  ],
  declarations: [KuarriPage, AddKuarriComponent]
})
export class KuarriPageModule { }
