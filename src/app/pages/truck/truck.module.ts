import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TruckPageRoutingModule } from './truck-routing.module';

import { TruckPage } from './truck.page';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyIonicModule } from '@ngx-formly/ionic';
import { AddTruckComponent } from './add-truck/add-truck.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TruckPageRoutingModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      extras: { lazyRender: true },
      validationMessages: [
        { name: 'required', message: '(*) This field is required' },
      ]
    }),
    FormlyIonicModule
  ],
  declarations: [TruckPage, AddTruckComponent]
})
export class TruckPageModule { }
