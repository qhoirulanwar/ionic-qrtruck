import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-read-truck',
  templateUrl: './read-truck.component.html',
  styleUrls: ['./read-truck.component.scss'],
})
export class ReadTruckComponent implements OnInit {

  @Input() data: any;
  id = Date.now()
  qrimg: string

  constructor(private modalController: ModalController) { }

  ngOnInit() {
    console.log(this.data);
    this.qrimg = `https://api.qrserver.com/v1/create-qr-code/?data=${this.data.qrcode}&amp;size=600x600`
  }

  openWin() {
    const myWindow = window.open('', '', 'width=900,height=700');
    myWindow.document.write(`
      <center>
        <img src="${this.qrimg}">
        <br>
        <br>
        <br>
        <h1>
        ${this.data.nopol}
        </h1>
      </center>`
    );

    setTimeout(() => {

      // myWindow.document.close();
      myWindow.focus();
      myWindow.print();
      // myWindow.close();

    }, 4000);
  }

}
