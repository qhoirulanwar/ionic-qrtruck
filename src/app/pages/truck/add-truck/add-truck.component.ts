import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CorporateService } from 'src/app/services/corporate/corporate.service';

@Component({
  selector: 'app-add-truck',
  templateUrl: './add-truck.component.html',
  styleUrls: ['./add-truck.component.scss'],
})
export class AddTruckComponent implements OnInit {

  constructor(private modalController: ModalController, private corpService: CorporateService) { }

  async ngOnInit() {
  }

  dataCorp = []

  form = new FormGroup({});
  model = {
    "nopol": null,
    "supir": null,
    "perusahaan": null,
    "index": null,
    "panjang": null,
    "lebar": null,
    "tinggi": null,
    "qrcode": Date.now()
  };
  fields: FormlyFieldConfig[] = [
    {
      key: 'nopol',
      type: 'input',
      templateOptions: {
        label: 'Nopol Kendaraan',
        // required: true,
        placeholder: 'Masukan Plat Kendaraan'
      }
    },
    {
      key: 'supir',
      type: 'input',
      templateOptions: {
        label: 'Nama Supir',
        placeholder: 'Masukan Nama Supir',
        type: 'text'
      }
    },
    {
      key: 'perusahaan',
      type: 'select',
      templateOptions: {
        label: 'Nama Perusahaan',
        placeholder: 'Pilih Perusahaan',
        type: 'text',
        options: this.corpService.getCorp(),
        valueProp: 'id',
        labelProp: 'name'
      }
    },
    {
      key: 'panjang',
      type: 'input',
      templateOptions: {
        label: 'Panjang',
        placeholder: 'Masukan Panjang Bak Kendaraan',
        type: 'number'
      }
    },
    {
      key: 'lebar',
      type: 'input',
      templateOptions: {
        label: 'Lebar',
        placeholder: 'Masukan Lebar Bak Kendaraan',
        type: 'number'
      }
    },
    {
      key: 'tinggi',
      type: 'input',
      templateOptions: {
        label: 'Tinggi',
        placeholder: 'Masukan Tinggi Bak Kendaraan',
        type: 'number'
      }
    },
    // {
    //   key: 'volume',
    //   type: 'input',
    //   templateOptions: {
    //     label: 'Volume',
    //     placeholder: 'Masukan Volume Bak Kendaraan',
    //     type: 'number'
    //   }
    // },
    // {
    //   key: 'qrcode',
    //   type: 'input',
    //   templateOptions: {
    //     label: 'QR Code',
    //     placeholder: 'Generate QR Code',
    //     type: 'text'
    //   }
    // }
  ];

  onSubmit() {
    this.model.index = this.model.panjang * this.model.lebar * this.model.tinggi

    console.log(this.model);
    console.log(this.form.valid);

    if (this.form.valid) {
      console.log('form not valid');
    }
  };

}
