import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CorporateService } from 'src/app/services/corporate/corporate.service';
import { TruckService } from 'src/app/services/truck/truck.service';
import { AddTruckComponent } from "./add-truck/add-truck.component";
import { ReadTruckComponent } from './read-truck/read-truck.component';

@Component({
  selector: 'app-truck',
  templateUrl: './truck.page.html',
  styleUrls: ['./truck.page.scss'],
})
export class TruckPage implements OnInit {
  form = new FormGroup({});
  fields: FormlyFieldConfig[]

  data: any = []

  constructor(
    public modalController: ModalController,
    public truckService: TruckService,
    public corpSrvc: CorporateService
  ) { }

  ngOnInit() {
    this.getData()
    this.fields = [
      {
        key: 'perusahaan',
        type: 'select',
        templateOptions: {
          label: 'Perusahaan Penyedia Angkutan',
          placeholder: 'Pilih Perusahaan',
          type: 'text',
          // required: true,
          options: this.corpSrvc.getCorp(),
          valueProp: 'id',
          labelProp: 'name',
          change: (field, $event) => {
            console.log(field.formControl.value);
            console.log($event.detail.value);
            // this.corpSrvc.getCorpById(field.formControl.value).subscribe((data) => {
            //   this.resultCorp = data
            //   console.log(data);
            // })
            this.truckService.getTruckByCorp(field.formControl.value).subscribe((data) => {
              this.data = data
              console.log(data);
            })
          },
        }
      }
    ]
  }

  async getData() {
    await (await this.truckService.getTruck()).subscribe((data: any) => {
      console.log(data);
      this.data = data
    })
  };

  async readModal(i) {
    //buka modal dengan data dengan index this.data[i]
    const modal = await this.modalController.create({
      component: ReadTruckComponent,
      cssClass: 'my-custom-class',
      componentProps: { data: this.data[i] }
    });
    return await modal.present();
  };

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddTruckComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  };

}
