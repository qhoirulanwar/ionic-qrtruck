import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TruckPage } from './truck.page';

describe('TruckPage', () => {
  let component: TruckPage;
  let fixture: ComponentFixture<TruckPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TruckPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TruckPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
