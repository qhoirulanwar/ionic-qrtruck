import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  panelWhen: string = 'md'
  menuLaporanLevel: boolean = false

  // TODO: laporan aktivitas scan (global)
  // TODO: laporan aktivitas proyek
  // TODO: laporan aktivitas suplayer

  // TODO: itung itungan /proyek
  // TODO: laporan suplayer / perusahaan / proyek atau / harian

  constructor(public menu: MenuController) { }

  ngOnInit() {
  }

  closeMenu(closePanel: boolean) {
    this.menu.close('first')
    console.log(closePanel);
    if (closePanel == true) {
      this.panelWhen = '(min-width: 1600px)'
    } else {
      this.panelWhen = 'md'
    }
  };

  menuLaporanLevelChange() {
    if (this.menuLaporanLevel == true) {
      this.menuLaporanLevel = false
    } else {
      this.menuLaporanLevel = true
    }
  }

}
