import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'admin',
    component: MenuPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../pages/home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'project',
        loadChildren: () => import('../pages/project/project.module').then(m => m.ProjectPageModule)
      },
      {
        path: 'corporation',
        loadChildren: () => import('../pages/corporation/corporation.module').then(m => m.CorporationPageModule)
      },
      {
        path: 'kuarri',
        loadChildren: () => import('../pages/kuarri/kuarri.module').then(m => m.KuarriPageModule)
      },
      {
        path: 'truck',
        loadChildren: () => import('../pages/truck/truck.module').then(m => m.TruckPageModule)
      },
      {
        path: 'report',
        loadChildren: () => import('../pages/report/report.module').then(m => m.ReportPageModule)
      },
      {
        path: 'checker',
        loadChildren: () => import('../pages/checker/checker.module').then(m => m.CheckerPageModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('../pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: '/app/admin/project'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule { }
